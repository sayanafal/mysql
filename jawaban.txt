1.membuat Database

 

create Database myshop

 

2.Membuat Table di Dalam Database	

Table kategori
CREATE TABLE kategori ( id int(8) PRIMARY KEY AUTO_INCREMENT, nama varchar(50) NOT null )

Table users

CREATE TABLE users( id int(8) AUTO_INCREMENT, nama varchar(30) NOT null, email varchar(30) NOT null, paswword varchar(30) NOT null, PRIMARY KEY(id) ) 

Table items

CREATE TABLE items( id int(8) AUTO_INCREMENT PRIMARY KEY, nama varchar(255) NOT null, Description varchar(255) NOT null, price int(9) NOT null, stock int(10), category_id int(8), FOREIGN KEY(category_id) REFERENCES kategori(id) ) 


3.memasukan data ke table

Table kategori INSERT INTO kategori VALUES ("gadget", "cloth", "men", "women", "branded"); 

Table users

INSERT INTO users(nama, email, paswword) VALUES ("John Doe", "john@doe.com", "john123");
INSERT INTO users(nama, email, paswword) VALUES ("Jane Doe", "jane@doe.com", "jenita123");

Table items

  INSERT INTO items(nama, Description, price, stock, category_id) VALUES ("Sumsang b50", "hape keren dari merek sumsang", "4000000", "100", "1");
  INSERT INTO items(nama, Description, price, stock, category_id) VALUES ("Uniklooh", "baju keren dari brand ternama", "500000", "50", "2");
  INSERT INTO items(nama, Description, price, stock, category_id) VALUES ("IMHO Watch", "jam tangan anak yang jujur banget", "2000000", "10", "1");   

4.mengambil data
  
     data users
  SELECT * FROM users WHERE Nama LIKE '%John Doe'
  SELECT * FROM users WHERE Nama LIKE '%john@doe.com' 
  SELECT * FROM users WHERE Nama LIKE '%Jane Doe'
  SELECT * FROM users WHERE Nama LIKE '%Jane Doe'

    data kategori
   SELECT * FROM kategori WHERE Nama LIKE '%gadget' 
   SELECT * FROM kategori WHERE Nama LIKE '%cloth'
   SELECT * FROM kategori WHERE Nama LIKE '%men'
   SELECT * FROM kategori WHERE Nama LIKE '%women'
   SELECT * FROM kategori WHERE Nama LIKE '%branded'
 
  data items 
   SELECT * FROM items WHERE Nama LIKE '%Sumsang b50' 
   SELECT * FROM items WHERE Nama LIKE '%Uniklooh' 
   SELECT * FROM items WHERE Nama LIKE '%'IMHO Watch' 
   

 5.mengubah data 
   UPDATE `items` SET `price` = 2500000 WHERE id=1 